const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

const DATABASE_USERNAME = 'admin';
const DATABASE_PASSWORD = 'admin123';
const DATABASE_NAME = 's35';

mongoose.set('strictQuery', true);
mongoose.connect(`mongodb+srv://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@clusterbatch248.riqw3pg.mongodb.net/${DATABASE_NAME}?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

// let db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error'));
// db.once('open', () => console.log("We're connected to MongoDB Atlas!"));

// const taskSchema = new mongoose.Schema({
//     name: {
//         type: String,
//         required: [true, 'Task name is required']
//     },
//     status: {
//         type: String,
//         default: 'Pending..'
//     }
// });

// const Task = mongoose.model('Task', taskSchema);
// app.post('/tasks', (req, res) => {
//     Task.findOne({name: req.body.name}, (err, result) => {
//         if (result != null && result.name == req.body.name) {
//             return res.send(`Duplicate task found!`)
//         } else {
//             let newTask = new Task({
//                 name: req.body.name
//             })
//             newTask.save((saveErr, savedTask) => {
//                 if (saveErr) {
//                     return console.error(saveErr);
//                 } else {
//                     console.log(`${savedTask} is saved!`);
//                     return res.status(201).send('New Task created!');
//                 }
//             })
//         }
//     })
// });

// app.get('/tasks', (req, res) => {
//     Task.find({}, (err, result) => {
//         if (err) {
//             console.log(err);
//         } else {
//             return res.send(result);
//         }
//     });
// });

// app.listen(port, () => console.log(`Server running at port ${port}`));


// const express = require("express");

//Mongoose is a package that allows creation of Schemas to model our data structures
//Mongoose is an ODM library to let our ExpressJS API manipulate a MongoDB database
// const mongoose = require("mongoose");

// const app = express();

// const port = 4000;

//MongoDB Connection

//connect to the database by pasing in our connection string
//remember to replace the password and database name with actual values

//Syntax
//mongoose.connect("<MongoDB Connection String>",{options to avoid errors in our connection})

//Connecting to MongoDB Atlas

// mongoose.set('strictQuery',true);

// mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.rkw6jep.mongodb.net/s35?retryWrites=true&w=majority",
// {
//     //Allows us to avoid any current and future errors while connecting to MongoDB
//     useNewUrlParser: true,
//     useUnifiedTopology: true
    
// })

//See notifications for connecton success or failure

//connection to database
//allows us to handle errors when the initial connection is established
//works with on and once Mengoose Methods

// let db = mongoose.connection;

// //error handling in connecting
// db.on("error",console.error.bind(console,"connection error"));
// //this will be triggered if the connection is successful
// db.once("open",()=>console.log("We're connected to MongoDB Atlas!"))

//Mongoose Schema
//Schema determine the structure of the documents to be written in the database
//Schemas act as blueprints to our data

//Syntax
//const schemaName = new mongoose.Schema({keyvaluepairs})

//use the Schema() constructir of the Mongoose module to create a new Schema object
//the "new" keyword creates a brand new Schema

//required, it is used to specify that a field must not be empty
//default, is used if a field value is not supplied

const taskSchema = new mongoose.Schema({
    
    name : {
        type: String,
        required: [true, "Task name is required!"]
    },
    status: {
        type: String,
        default: "pending"
    }
})

//Models
//uses schema and are used to create/instantiate objects that correspond to our schema
//Models use Schema and they acta as middleman from the server to our database
//Server>Schema(blueprint)>Database>Collection

//first parameter - collection where to store the data
//second parameter - specify the Schema/blueprint of the documents that will be stored in our db

const Task = mongoose.model("Task",taskSchema)


//Setup for allowing the server to handle data from requests
// //allows your application to read json data
// app.use(express.json());
// //allow our app to read data from forms
// app.use(express.urlencoded({extended:true}));

//Creating a new task

//Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
-if the task already exists in the db, we will return an error
-if the task doesn't exist in the db, we add it in the db
2. The task data will be coming from our request body
3. Create a new Task object with only a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks",(req,res)=>{
    
    
    /*
    Check if there are duplicate tasks
    "findOne" is a Mongoose method that acts similar to "find" of MongoDB
    findOne() returns the first document that matches the search criteria
    If there are no matches, the value of result is null
    ***"err" is a shorthand naming convention for errors
    */
    
    
    Task.findOne({name:req.body.name},(err,result)=>{
        
        //If a document was found and the document's name matches the information sent via the client/Postman
        
        if(result != null && result.name == req.body.name){
            // Return a message to the client/Postman
            return res.send(`Duplicate task found!`)
            
            // If no document was found
        }else{
            
            // Create a new task and save it to the database
            let newTask = new Task({
                name: req.body.name
            })
            
            // The "save" method will store the information to the database
            // Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
            // The "save" method will accept a callback function which stores any errors found in the first parameter
            // The second parameter of the callback function will store the newly saved document
            // Call back functions in mongoose methods are programmed this way to store any errors in the first parameter and the returned results in the second parameter
            
            newTask.save((saveErr,savedTask)=>{
                
                // If there are errors in saving
                if(saveErr){
                    // Will print any errors found in the console
                    // saveErr is an error object that will contain details about the error
                    // Errors normally come as an object data type
                    return console.error(saveErr)
                    // No error found while creating the document
                }else{
                    
                    // Return a status code of 201 for created
                    // Sends a message "New task created" on successful creation
                    console.log("Task is saved!");
                    return res.status(201).send("New Task created")
                }
            })
        }
    })
})

app.get('/tasks', (req, res) => {
    Task.find({}, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            return res.send(result);
        }
    });
});





//Activity

// Registering a user
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
- If the user already exists in the database, we return an error
- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/





//listens to the port, meaning, if the port is accessed, we run the server
app.listen(port,()=>console.log(`Server running at port ${port}`));