const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

const DATABASE_USERNAME = 'admin';
const DATABASE_PASSWORD = 'admin123';
const DATABASE_NAME = 's35';

mongoose.set('strictQuery', true);
mongoose.connect(`mongodb+srv://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@clusterbatch248.riqw3pg.mongodb.net/${DATABASE_NAME}?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log("We're connected to MongoDB Atlas!"));

//1. Create a User schema.
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'username is required']
    },
    password: {
        type: String,
        required: [true, 'password is required']
    }
});

//2. Create a User model.
const User = mongoose.model('User', userSchema);

//3. Create a POST route that will access the "/signup" route that will create a user.
app.post('/signup', (req, res) => {
    User.findOne({username: req.body.username, password: req.body.password}, (err, result) => {
        if (!req.body.username || !req.body.password){
            return res.send(`Username and password must be provided!`);
        } else if (result != null && result.username == req.body.username) {
            return res.send(`Duplicate username found!`);
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })
            newUser.save((saveErr, savedUser) => {
                if (saveErr) {
                    return console.error(saveErr);
                } else {
                    console.log(`${savedUser} is saved!`);
                    return res.status(201).send('New User Registered!');
                }
            })
        }
    })
})

app.listen(port, () => console.log(`Server running at port ${port}`));
